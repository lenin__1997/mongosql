package com.codeboard.mongosql.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.codeboard.mongosql.model.UserSql;

@Repository
public interface SqlRepository extends JpaRepository<UserSql, Integer> {

}
