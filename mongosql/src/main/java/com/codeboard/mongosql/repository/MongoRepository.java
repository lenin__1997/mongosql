package com.codeboard.mongosql.repository;
import org.springframework.stereotype.Repository;

import com.codeboard.mongosql.model.UserMongo;

@Repository
public interface MongoRepository extends org.springframework.data.mongodb.repository.MongoRepository<UserMongo, String> {

}
