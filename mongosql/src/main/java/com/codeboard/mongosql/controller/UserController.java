package com.codeboard.mongosql.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.codeboard.mongosql.model.UserMongo;
import com.codeboard.mongosql.model.UserSql;
import com.codeboard.mongosql.repository.MongoRepository;
import com.codeboard.mongosql.repository.SqlRepository;

@RestController
@CrossOrigin("http://localhost:4200/")
public class UserController {

	@Autowired
	private MongoRepository mongoRepository;

	@Autowired
	private SqlRepository sqlRepository;
	

	@PostMapping("/p/s")
	public UserSql postS(@RequestBody UserSql userSql) {
		return sqlRepository.save(userSql);
	}
	
	@PostMapping("/p/m")
	public UserMongo postM(@RequestBody UserMongo userDoc) {
		return mongoRepository.save(	userDoc);
	}

	@GetMapping("/{db}")
	public List<?> getDb(@PathVariable("db") int db) {
		List<?> user = new ArrayList<>();
		if (db == 1) {
			user = sqlRepository.findAll();
		} else if (db == 2) {
			user = mongoRepository.findAll();
		}
		return user;
	}

}
