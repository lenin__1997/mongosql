package com.codeboard.mongosql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MongosqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(MongosqlApplication.class, args);
	}

}
