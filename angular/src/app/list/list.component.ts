import { Component } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../user';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent {
  user : User[] =[];
  db!: number
  c =true;
  constructor(private userService:UserService){}

  ngOnInit(){
    // this.userService.getUser().subscribe(data=>this.user=data)
    this.changeDb()
  }
  changeDb(){
    // this.c=!this.c
    this.c = this.c ? false : true
    if(this.c){
      
      this.db=1;
    }
    else{
      this.db=2;
    }
    this.userService.getUserdb(this.db).subscribe(data=>this.user=data)
  }
}
