import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, map } from 'rxjs';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) { }

  // getUser(): Observable<User[]>{
  //   return this.httpClient.get<User[]>(`${'http://localhost:8080/2'}`)
  // }
  getUserdb(db:Number): Observable<User[]>{
    return this.httpClient.get<User[]>(`${'http://localhost:8080'}/${db}`)
  }
}

